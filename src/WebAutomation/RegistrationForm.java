package WebAutomation; 

import WebAutomation.Utilities;
import java.util.concurrent.TimeUnit;
import java.lang.Object;

import java.util.*;
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.Assert;
import org.testng.annotations.*;

public class RegistrationForm {

	Utilities ut;
	public static WebDriver driver;// = new FirefoxDriver();
	String url;
	String username = "fullname";
	String email = "email";
	String mobile_number = "mobnumber";
	//String loginLink = 
	public RegistrationForm(String browser){
		System.setProperty("webdriver.chrome.driver", "C://Anupama/Softwares/chromedriver_win32/chromedriver.exe");
		switch(browser){
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Chrome":
			driver = new ChromeDriver();
			break;
			
		}
		url = "http://capitalfloat.com";
	}

	public void openSite(){
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	public boolean checkSite(String title){
		//String expectedTitle = "Capital Float - Smart Business loans for SMEs in India";
		String actualTitle = driver.getTitle();
		return title == actualTitle;
	}
	
	public void openLoginPage(){
		driver.findElement(By.linkText("LOG IN")).click();
		if(!checkSite("Login")){
			//log error that unable to open page
		}
	}
	
	public void openRegistrationPage(){
		driver.findElement(By.linkText("Register Now")).click();
		if(!checkSite("Register")){
			//log error that unable to open page
		}
	}
	
	@BeforeTest
	public void setUp(){
		System.setProperty("webdriver.chrome.driver", "C://Anupama/Softwares/chromedriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://capitalfloat.com");
		driver.manage().window().maximize();
		driver.findElement(By.linkText("LOG IN")).click();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Register Now")).click();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		ut = new Utilities();
		/*
		RegistrationForm formChr = new RegistrationForm("Chrome");
		formChr.openSite();
		formChr.openLoginPage();
		formChr.openRegistrationPage();*/
	}
	/*
	@AfterTest
	public void cleanUp(){
		driver.quit();
	}*/
	
	/*
	
	@Test
	public void nameNum(){
		ut.fillFullName("123456");
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "");
	}
	
	@Test
	public void nameAlphaNum(){
		ut.fillFullName("12abhgt3r456");
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "");
	}
	
	@Test
	public void nameNull(){
		ut.fillFullName("test");
		ut.fname.clear();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "please enter your full name");
	}
	
	@Test
	public void emailNull(){
		ut.fillEmail("test@gmail.com");
		ut.email.clear();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpEmail(), "please enter your email address");
	}
	
	@Test
	public void emailDomain(){
		ut.fillEmail("123456789@gmail");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "");	
	}
	
	@Test
	public void emailInvalid(){
		ut.fillEmail("test");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "please enter your email in proper format");	
	}
	
	@Test
	public void email254char(){
		//ut.fillEmail("dfrthhhhyffrmhdfkvkjkhlfkkhjvnhxjfksjchgskslgcnhkgxckhksgkhgkksfhngfjfxxgshkhjxfghksjghjbcxkckgkxnrxgtxgxzmdfrthhhhyffrmhdfkvkjkhlfkhkhjvnhxjfksjchgskslgcnhkgxckhksgkhgkksfhngfjfxxgshkhjxfghksjghjbcxkckgkxnrxgtxgxzmkljckghkfgnxkrkozlhgmkhjgrkhz@hgkjgjjhj");
		ut.fillEmail("dfrthhhhyffrmhdfkvkjkhlfkkhjvnhxjfksjchgskslgcnhkgxckhksgk"
				+ "hgkksfhngfjfxxgshkhjxfghksjghjbcxkckgkxnrxgtxgxzmdfrthhhhyffr"
				+ "mhdfkvkjkhlfkhkhjvnhxjfksjchgskslgcnhkgxckhksgkhgkksfhngfjfxxg"
				+ "shkhjxfghksjghjbcxkckgkxnrxgtxgxzmkljckghkfgnxkrkozlhgmkhjgrkh"
				+ "z@hgkjgjjhj");
		
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "");	
	}
	
	@Test
	public void emailInvalid(){
		ut.fillEmail("#$^&6887@g$ail.com");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "please enter your email in proper format");	
	}
	*/
	@Test
	public void mobNull(){
		ut.mNumber.clear();
		ut.fillMobNumber("");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "please provide your mobile number");	
	}
	
	@Test
	public void mobAlphaNum(){
		ut.fillMobNumber("9abcd$rf78");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "please enter a valid number.");	
	}
	/*
	@Test
	public void mobStart0(){
		ut.fillMobNumber("0123456787");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "mobile number has to be a minimum of 10 digits");	
	}
	
	@Test
	public void mobValid(){
		ut.fillMobNumber("1123456787");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "");	
	}
	
	
	/*
	
	
	*/
	/*
	@Test
	public void testing() throws Exception{
		
		ut.setValues(driver, "Hello", "test@gmail.com", "9876432102", null, 
				"Private Limited", null, 3, 30, "yes");
		driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		ut.chooseMarketChkBox(driver, "myntra");
		
	}
	
	*/
	
	/*
	@Test
	public void nameNull() throws Exception{
		
		
		driver.findElement(By.name("fullname")).sendKeys("56781234");
		driver.findElement(By.id(email)).sendKeys("test@gmail.com");
		driver.findElement(By.id(mobile_number)).sendKeys("987643210");
		driver.findElement(By.id("city")).click();
		driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[4]/div/select/option[3]")).click();
		driver.findElement(By.id("operation")).click();
		driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[5]/div/select/option[3]")).click();
		driver.findElement(By.id("businessname")).click();
		driver.findElement(By.id("businessname")).sendKeys("name");
		driver.findElement(By.id("company_turnover")).click();
		driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[7]/div/select/option[4]")).click();
		
		//WebElement slider = driver.findElement(By.className("sliderHolder"));
		WebElement slider = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[8]/div/div[3]"));
		
		if(slider.isDisplayed()){
			System.out.println("Found slider");
		}
		Actions move = new Actions(driver);
		Point k = slider.getLocation();
		System.out.println("Slider location: " + k.x + " " + k.y);
	    move.moveToElement(slider, 551, 509);
		move.clickAndHold();
		move.dragAndDropBy(slider, k.x+10000, k.y).build().perform();
		//move.moveByOffset(90, 0).build().perform();
		//Action action = move.dragAndDropBy(slider, 60, 0).build();
		//action.perform();
		
	}*/
	
	
	

}
